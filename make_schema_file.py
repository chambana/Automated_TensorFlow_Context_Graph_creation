#!/usr/bin/python
import csv

# CLASS_LABELS_PATH = 'imagenet_class_list.csv'
# SCHEMA_FILE_PATH = 'imagenet_context_graph_schema.sql'
CLASS_LABELS_PATH = 'coco_id_to_label.csv'
SCHEMA_FILE_PATH = 'coco_context_graph_schema.sql'


print "**************** Creating Schema from CSV ********************"

labels_file = open(CLASS_LABELS_PATH, "r")
schema_file = open(SCHEMA_FILE_PATH, "w")

csv_object = csv.reader(labels_file)
for row in csv_object:
    print row
    entry_to_write = 'CREATE TABLE `%s` (NEIGHBOR_SYNSET_NUMBER CHAR(9), NUM_OF_INSTANCES INTEGER(10));\n' % row[0]
    schema_file.write(entry_to_write)

labels_file.close()


labels_file = open(CLASS_LABELS_PATH, "r")
csv_object = csv.reader(labels_file)

entry_to_write = 'CREATE TABLE %s (SYNSET_NUMBER CHAR(9), NAME CHAR(150));\n' % 'CLASSNAME_LOOKUP'
schema_file.write(entry_to_write)

for row in csv_object:
    print row
    entry_to_write = 'INSERT INTO CLASSNAME_LOOKUP VALUES ("%s", "%s");\n' % (row[0],row[1])
    schema_file.write(entry_to_write)


labels_file.close()
schema_file.close()

