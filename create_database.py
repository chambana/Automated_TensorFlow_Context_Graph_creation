#!/usr/bin/python
import MySQLdb
from object_detection_class import *


CONFIDENCE_THRESHOLD_TO_ADD = 0.90
schema_file = "coco_context_graph_schema.sql"


def execute_query(curs, sql_query):
    result = None
    try:
        result = curs.execute(sql_query)
    except Exception as e:
        print str(e)
    return result

def load_schema(schema_file_name):

    PATH_TO_CONTEXT_GRAPH_SCHEMA = schema_file_name

    creds = "dontcommit"
    password = open(creds).read()

    db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                         user="root",         # your username
                         passwd=password)  # your password
                         # db="mysql")        # name of the data base

    db.autocommit(True)
    cur = db.cursor()

    #
    # query = 'DROP DATABASE IF EXISTS CONTEXT_GRAPH'   # CAREFUL NOT TO CLEAR YOUR GRAPH!!!!!!
    #
    # execute_query(cur, query)
    #
    '''
    ************* LOAD SCHEMA FOR CONTEXT GRAPH **************
    '''
    query = "SHOW DATABASES LIKE 'CONTEXT_GRAPH'" # (WHICH INCLUDES THE LOOKUP TABLE)
    resp = cur.execute(query)
    if resp:
        print "CONTEXT_GRAPH db already exists, skipping loading of schema"
        query = "USE CONTEXT_GRAPH"
        cur.execute(query)
    else:
        print "CONTEXT_GRAPH db not found, creating and loading schema"

        query = "CREATE DATABASE IF NOT EXISTS CONTEXT_GRAPH"
        cur.execute(query)

        query = "USE CONTEXT_GRAPH"
        cur.execute(query)


        file = open(PATH_TO_CONTEXT_GRAPH_SCHEMA)
        commands = file.read()
        commands = commands.split(';')

        for item in commands:
            print "\nDEBUG, exec'ing", repr(item)
            if item=='\r\n\r\n' or item=='' or item=='\n':
                print "breaking"
                break
            cur.execute(item)


    '''
    ************  CLEANUP **************
    '''
    print "closing db"
    db.close()



def get_label_table():
    creds = "dontcommit"
    password = open(creds).read()

    db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                         user="root",  # your username
                         passwd=password)  # your password
    # db="mysql")        # name of the data base

    db.autocommit(True)
    cur = db.cursor()

    query = "USE CONTEXT_GRAPH"
    cur.execute(query)

    query = "SELECT * from CLASSNAME_LOOKUP"
    cur.execute(query)

    result = cur.fetchall()

    db.close()
    return result


def populate_tables():
    creds = "dontcommit"
    password = open(creds).read()

    db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                         user="root",  # your username
                         passwd=password)  # your password
    # db="mysql")        # name of the data base

    db.autocommit(True)
    cur = db.cursor()

    query = "USE CONTEXT_GRAPH"
    execute_query(cur, query)

    print "GETTING LABEL TABLE"
    label_table = get_label_table()
    print label_table


    #iterate through all files in subdirs of path images/
    detector = ObjectDetection()

    for entry in label_table:
        readable_label = entry[1]
        print "Processing class", readable_label
        dir_to_process = 'images/'+readable_label
        print "in path", dir_to_process

        identification_results = detector.run(dir_to_process)

        #identification results format:   [(boxes, scores, classes, num_detections), (boxes, scores, classes, num_detections), (boxes, scores, classes, num_detections)]

        print "len of identification results", len(identification_results)

        for result in identification_results:
            #result format (boxes, scores, classes, num_detections)
            print "entry[0]", entry[0]
            boxes = result[0][0]
            scores = result[1][0]
            classes = result[2][0]
            num_detections = result[3]
            print "scores", scores[0:3]
            print "classes", classes[0:3]



            #check if the first "hit" is strong enough to be added to the relationship table
            if scores[0] > CONFIDENCE_THRESHOLD_TO_ADD:
                print "RELATIONSHIP BETWEEN", entry[0],"AND", int(classes[0]),"IS STRONG ENOUGH, ADDING"

                #determine if a row for this class is already in this table
                query = "SELECT * FROM `%s` WHERE NEIGHBOR_SYNSET_NUMBER=%i" % (entry[0], int(classes[0]))
                execute_query(cur, query)
                values = cur.fetchall()

                #FIRST INSTANCE, ADD NEW ROW
                if len(values)==0:
                    print "No value found in DB, adding..."
                    query = 'INSERT INTO `%s` VALUES ("%i",1)' % (entry[0],int(classes[0]),)
                    execute_query(cur, query)
                #ENTRY ALREADY IN TABLE, UPDATE IT BY ADDING 1
                else:
                    print "Increment # of instances by 1 in DB.."
                    query = 'UPDATE `%s` SET NUM_OF_INSTANCES = NUM_OF_INSTANCES + 1 WHERE NEIGHBOR_SYNSET_NUMBER = %i' % (entry[0], int(classes[0]))
                    execute_query(cur, query)
            else:
                print "RELATIONSHIP BETWEEN", entry[0],"AND", int(classes[0]),"NOT STRONG ENOUGH, DROPPING"

            #check if the second "hit" is strong enough to be added to the relationship table
            if scores[1] > CONFIDENCE_THRESHOLD_TO_ADD:
                print "RELATIONSHIP BETWEEN", entry[0], "AND", int(classes[1]), "IS STRONG ENOUGH, ADDING"

                # determine if a row for this class is already in this table
                query = "SELECT * FROM `%s` WHERE NEIGHBOR_SYNSET_NUMBER=%i" % (entry[0], int(classes[1]))
                execute_query(cur, query)
                values = cur.fetchall()

                # FIRST INSTANCE, ADD NEW ROW
                if len(values) == 0:
                    print "No value found in DB, adding..."
                    query = 'INSERT INTO `%s` VALUES ("%i",1)' % (entry[0], int(classes[1]),)
                    execute_query(cur, query)
                # ENTRY ALREADY IN TABLE, UPDATE IT BY ADDING 1
                else:
                    print "Increment # of instances by 1 in DB.."
                    query = 'UPDATE `%s` SET NUM_OF_INSTANCES = NUM_OF_INSTANCES + 1 WHERE NEIGHBOR_SYNSET_NUMBER = %i' % (entry[0], int(classes[1]))
                    execute_query(cur, query)
            else:
                print "RELATIONSHIP BETWEEN", entry[0], "AND", int(classes[1]), "NOT STRONG ENOUGH, DROPPING"


    db.close()



if __name__ == "__main__":

    print "******** STARTING CONTEXT DATABASE CREATION *********"

    # print "STEP 1:  Loading Schema into SQL Database"
    # load_schema(schema_file)


    print "STEP 2:  Detecting Objects in Images and Populating Tables"
    populate_tables()








