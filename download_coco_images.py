from image_downloader import *
import csv


downloader_object = ImageDownloader()

CLASS_LABELS_PATH = 'coco_id_to_label.csv'
labels_file = open(CLASS_LABELS_PATH, "r")

csv_object = csv.reader(labels_file)

for row in csv_object:
    print row[1]
    try:
        if not os.path.exists('images/'+row[1]):
            downloader_object.run([row[1]])
    except Exception as e:
        print str(e)
        print "continuing..."
