
# coding: utf-8

# Uses code from TensorFlow Object Detection API tutorial


'''
run from terminal with this first
export PYTHONPATH=$PYTHONPATH:/home/drew/models/object_detection
'''

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile
from collections import defaultdict
from io import StringIO
# from matplotlib import pyplot as plt
from PIL import Image
import csv
from utils import label_map_util


#DISABLE DECOMPRESSION BOMB ATTACK PROTECTION
Image.MAX_IMAGE_PIXELS = None


sys.path.append("..")




MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
# MODEL_NAME = 'faster_rcnn_inception_resnet_v2_atrous_coco_11_06_2017'
MODEL_FILE = MODEL_NAME + '.tar.gz'
DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'
# faster_rcnn_inception_resnet_v2_atrous_coco
# Path to frozen detection graph. This is the actual model that is used for the object detection.
PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
# List of the strings that is used to add correct label for each box.
PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
NUM_CLASSES = 90


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

def make_lookup_dict(path):
    result = {}
    labels_file = open(path, "r")
    csv_object = csv.reader(labels_file)
    for row in csv_object:
        result[row[0]]=row[1]
    return result

class ObjectDetection(object):

    def __init__(self, download_model=False):

        CLASS_LABELS_PATH = 'coco_id_to_label.csv'
        self.lookup_table = make_lookup_dict(CLASS_LABELS_PATH)

        if download_model:
            opener = urllib.request.URLopener()
            opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
            tar_file = tarfile.open(MODEL_FILE)
            for file in tar_file.getmembers():
              file_name = os.path.basename(file.name)
              if 'frozen_inference_graph.pb' in file_name:
                tar_file.extract(file, os.getcwd())


    # TAKES A LOCAL PATH, NOT FILE NAMES
    def run(self, local_path):
        detection_graph = tf.Graph()
        with detection_graph.as_default():
          od_graph_def = tf.GraphDef()
          with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')



        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
        category_index = label_map_util.create_category_index(categories)


        PATH_TO_TEST_IMAGES_DIR = local_path
        TEST_IMAGE_PATHS = [f for f in os.listdir(PATH_TO_TEST_IMAGES_DIR) if os.path.isfile(os.path.join(PATH_TO_TEST_IMAGES_DIR, f))]
        for i in range(0, len(TEST_IMAGE_PATHS)):
            TEST_IMAGE_PATHS[i] = PATH_TO_TEST_IMAGES_DIR + '/' + TEST_IMAGE_PATHS[i]
        print TEST_IMAGE_PATHS
        # Size, in inches, of the output images.
        IMAGE_SIZE = (12, 8)


        results_tuple_list = []
        i =-1

        with detection_graph.as_default():
            with tf.Session(graph=detection_graph) as sess:

                for image_path in TEST_IMAGE_PATHS:

                    i+=1

                    print "i=", i

                    try:
                        image = Image.open(image_path)
                        print image_path
                    except:
                        print "couldnt open file",image_path,"skipping"
                        continue
                    # the array based representation of the image will be used later in order to prepare the
                    # result image with boxes and labels on it.
                    try:
                        image_np = load_image_into_numpy_array(image)
                    except Exception as e:
                        print "Couldnt reshape, continuing"
                        continue
                    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
                    try:
                        image_np_expanded = np.expand_dims(image_np, axis=0)
                    except Exception as e:
                        print "couldnt expand, continuing"
                        continue
                    try:
                        print "starting tf stuff"
                        image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
                        # Each box represents a part of the image where a particular object was detected.
                        boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
                        # Each score represent how level of confidence for each of the objects.
                        # Score is shown on the result image, together with the class label.
                        scores = detection_graph.get_tensor_by_name('detection_scores:0')
                        classes = detection_graph.get_tensor_by_name('detection_classes:0')
                        num_detections = detection_graph.get_tensor_by_name('num_detections:0')
                        # Actual detection.
                        (boxes, scores, classes, num_detections) = sess.run(
                            [boxes, scores, classes, num_detections],
                            feed_dict={image_tensor: image_np_expanded})

                        results_tuple_list.append((boxes, scores, classes, num_detections))
                    except Exception as e:
                        print "something happened"
                        continue


                    print "****EVALUATING", image_path, "as classes", self.lookup_table[str(int(classes[0][0]))], self.lookup_table[str(int(classes[0][1]))], "with scores", scores[0][0], scores[0][1]
        print "******RETURNING******"
        return results_tuple_list

if __name__ == "__main__":
    print "testing object detection"

    detector = ObjectDetection()
    results = detector.run('Australia')


