
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.ticker as ticker
import numpy as np
import MySQLdb
from create_database import  *

label_dict={}
NUM_PHOTOS_PER_CATEGORY = 100

def convert_label_table_to_dict(table):
    new_dict = {}
    for item in table:
        new_dict[int(item[0])]=item[1]
    return new_dict

def scrape_database():
    creds = "dontcommit"
    password = open(creds).read()

    db = MySQLdb.connect(host="localhost",  # your host, usually localhost
                         user="root",  # your username
                         passwd=password)  # your password
    # db="mysql")        # name of the data base

    db.autocommit(True)
    cur = db.cursor()

    query = "USE CONTEXT_GRAPH"
    execute_query(cur, query)

    print "GETTING LABEL TABLE"
    label_table = get_label_table()

    global label_dict
    label_dict= convert_label_table_to_dict(label_table)

    scraped_database = {}

    for category in sorted(label_dict):
        print "SCRAPING", category
        query = "select * from `%i`" % (category)
        execute_query(cur, query)
        results = cur.fetchall()
        #results format: (('1', 16), ('32', 1))

        if len(results) != 0:
            scraped_database[category]={}
            for entry in results:
                scraped_database[category][int(entry[0])]=entry[1]

    db.close()
    return scraped_database


def plot_scatter(dict_of_database):
    num_categories = len(label_dict)
    # print num_categories
    multipler =  1. / NUM_PHOTOS_PER_CATEGORY
    print multipler

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # for category_index in range(0, len(dict_of_database)):
    for category_index in dict_of_database:
        print "working", category_index
        for key in dict_of_database[category_index]:
            x=category_index
            y=key
            z=dict_of_database[category_index][key]
            print "color", [(multipler * z,0,0)]
            ax.scatter(x,y,z, c=[(multipler*z, 0,0)], edgecolor='none', s=150)

    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    ax.set_xlim(0, max(dict_of_database.keys()))
    ax.set_ylim(0, max(dict_of_database.keys()))
    ax.set_zlim(0, NUM_PHOTOS_PER_CATEGORY)

    plt.show()


def plot_bar(dict_of_database, plot_hits_on_self=True):
    fig = plt.figure(figsize=(50, 40))
    ax1 = fig.add_subplot(111, projection='3d')

    num_categories = len(label_dict)
    # print num_categories
    multipler =  1. / 5
    print multipler

    # for category_index in range(0, len(dict_of_database)):
    for category_index in sorted(dict_of_database):
    # for category_index in range(1,2):

        print "working", category_index
        cat_color =  np.random.rand(1)[0]
        for key in sorted(dict_of_database[category_index]):
            if (category_index == key) and not plot_hits_on_self:
                continue
            x=category_index
            y=key
            z=dict_of_database[category_index][key]
            ax1.bar3d(x, y, 0, 0.5, 0.5, z, color=[(min(multipler*z,1),min(multipler*z,1),min(multipler*z,1))])

    ax1.set_xlabel('Category Number')
    ax1.set_ylabel('Category Number')
    ax1.set_zlabel('Number of Instances')
    ax1.set_xlim(0, max(dict_of_database.keys()))
    ax1.set_ylim(0, max(dict_of_database.keys()))
    ax1.set_zlim(bottom=0)

    label_ticks = []
    for key in range(1, len(label_dict.keys())):
        if key in label_dict.keys():
            label_ticks.append(label_dict[key])
        else:
            label_ticks.append('')

    plt.xticks(np.arange(0, max(dict_of_database.keys())+1, 1.0),fontsize='6',rotation=45,rotation_mode="anchor")
    plt.yticks(np.arange(0, max(dict_of_database.keys())+1, 1.0),fontsize='6',rotation=45,rotation_mode="anchor")

    counter=0
    step_size = 1.0/90.0
    for key in label_dict:
        plt.figtext(0.005, 0.91 - (counter * step_size), str(key) +' - ' + str(label_dict[key]))
        counter+=1

    plt.title("Visualization of Relationship Database", fontsize=40)
    plt.tight_layout()

    plt.show()


if __name__ == "__main__":


    print "SCRAPING DATABASE"
    scraped = scrape_database()

    # plot_scatter(scraped)
    plot_bar(scraped, plot_hits_on_self=True)

    print scraped




